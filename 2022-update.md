Almost half a year has passed since the last update blog post. Sorry for that! What we've lacked in public relations, we make up in development. Because this one is huge.

When playing modern games, they often have that "punch" in how it looks. The lighting effects, the colors and so on. This is often done through post-processing shaders, something MGE XE users have been enjoying for a long time already while OpenMW users simply had to imagine they had nice things like bloom, hdr, ambient occlusion, godrays, motion blur -- all while the game in reality looked dull and boring. Until now that is.

While our legend AnyOldName3 was once knighted to Sir AnyOldName3, Master of Shadows, after his amazing work on our real-time shadows, today is the day to knight wazabear to [Sir Wazabear, Master of Lights](https://gitlab.com/OpenMW/openmw/-/merge_requests/1124). Because oh boy is this a game changer! 

We now have a new file format called omwfx where the post-processing shaders are stored. This means shader developers have an easy way to make the shaders and end-users have an easy way to install them into their game. Each shader can be turned on and off in one click and even fine-tuned through the very handy in-game interface. The shaders can also be "hot reloaded", meaning you can even edit the shaders while the game is running. This is of course very handy for developers so that they can see the result of what they just wrote in real time.

One shader developer, zesterer, has already made his contribution to the community. Have a look at some screenshots [here](add link to some imgur album or something)!

## More advanced launcher

Speaking of easy ways for end users to install things, our launcher now supports adding and removing mod directories so that you can do it in a GUI way instead of only through cfg files. A highly requested feature now delivered to you from 

## But what about Lua?

Yes of course we should talk about Lua. Much has happened since last time and some mods have even been released, but Lua is *still* not ready for its real prime-time yet. More of the game needs to be de-hardcoded until we get to the see amazing stuff similar to what MWSE-Lua allows today. 

## 
