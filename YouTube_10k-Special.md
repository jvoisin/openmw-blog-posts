# OpenMW Special Video: 10,000 YouTube Subscribers

Starting with version 0.13.0, each and every main release of our engine has been accompanied by a release commentary. For me and many others, the release videos have always been something to look forward to, a strangely satisfying means to highlight OpenMW's progress throughout the ages -- and a great opportunity to mess around with our beloved Fargoth!

Our channel has now reached 10,000 subscribers, a truly gigantic number for such a niche project like OpenMW. A huge thank-you to all of you for your continuous support! Rest assured, though, that we are not reaching for the stars here and that we will still concentrate on publishing release commentaries in the future instead of featuring puppies or kittens. But a little celebration seems to be appropriate, which is why I present you with an updated version of our 2013 video "A Visual History of Changes to OpenMW"! Sit back, relax, and have fun. -- And thanks again for your support!

https://www.youtube.com/watch?v=UeJc3e_qQWY